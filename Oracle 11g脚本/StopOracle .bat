@echo off

echo 获取Administrator权限
cacls.exe "%SystemDrive%\System Volume Information" >nul 2>nul
if %errorlevel%==0 goto Admin
if exist "%temp%\getadmin.vbs" del /f /q "%temp%\getadmin.vbs"
echo Set RequestUAC = CreateObject^("Shell.Application"^)>"%temp%\getadmin.vbs"
echo RequestUAC.ShellExecute "%~s0","","","runas",1 >>"%temp%\getadmin.vbs"
echo WScript.Quit >>"%temp%\getadmin.vbs"
"%temp%\getadmin.vbs" /f
if exist "%temp%\getadmin.vbs" del /f /q "%temp%\getadmin.vbs"
exit

:Admin

echo 成功取得Administrator权限

echo 1-检测OracleMTSRecoveryService状态
for /f "skip=3 tokens=4" %%i in ('sc query OracleMTSRecoveryService') do set "zt=%%i" &goto :next
:next
if /i "%zt%"=="RUNNING" (
    echo OracleMTSRecoveryService服务运行中 
    echo 停止OracleMTSRecoveryService
    net stop OracleMTSRecoveryService
) else (
    echo 该服务现在处于停止状态
)

echo 2-检测OracleDBConsoleorcl状态
for /f "skip=3 tokens=4" %%i in ('sc query OracleDBConsoleorcl') do set "zt=%%i" &goto :next
:next
if /i "%zt%"=="RUNNING" (
    echo OracleDBConsoleorcl服务运行中
    echo 停止OracleDBConsoleorcl
    net stop OracleDBConsoleorcl
) else (
    echo 该服务现在处于停止状态
)

echo 3-检测OracleOraDb11g_home1TNSListener状态
for /f "skip=3 tokens=4" %%i in ('sc query OracleOraDb11g_home1TNSListener') do set "zt=%%i" &goto :next
:next
if /i "%zt%"=="RUNNING" (
    echo OracleOraDb11g_home1TNSListener服务运行中
    echo 停止OracleOraDb11g_home1TNSListener
    net stop OracleOraDb11g_home1TNSListener
) else (
    echo 该服务现在处于停止状态
)

echo 4-检测OracleServiceORCL状态
for /f "skip=3 tokens=4" %%i in ('sc query OracleServiceORCL') do set "zt=%%i" &goto :next
:next
if /i "%zt%"=="RUNNING" (
    echo OracleServiceORCL服务运行中
    echo 停止OracleServiceORCL
    net stop OracleServiceORCL
) else (
    echo 该服务现在处于停止状态
)

echo 停止Oracle 11g服务完成，请确认有没有错误发生。

Pause